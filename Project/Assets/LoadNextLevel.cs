﻿using UnityEngine;
using System.Collections;

public class LoadNextLevel : MonoBehaviour {

    public string lostSceneName="GameOver";
    public string winSceneName = "Credit";
    public float timeBeforeLoadItWin = 3f;
    public float timeBeforeLoadItLose = 1f;
    public GameObject[] activateOnWin;
    public GameObject[] activateOnLost;

    void OnEnable()
    {
        if (CurrentLevelManager.InstanceInScene)
        {
            CurrentLevelManager.InstanceInScene.onLevelWin += YouWIN;
            CurrentLevelManager.InstanceInScene.onLevelLost += YouLOST;
        }
    }

    private void YouLOST(float timeLeft)
    {
        foreach (GameObject gamo in activateOnLost)
            if(gamo)gamo.SetActive(true);
        Invoke("LoadLost", timeBeforeLoadItLose);
    }

    private void YouWIN(float timeLeft)
    {

        Debug.Log("Win");
        foreach (GameObject gamo in activateOnWin)
            if (gamo) gamo.SetActive(true);
        Invoke("LoadWin", timeBeforeLoadItWin);
    }

    private void LoadWin() 
    {
        if (Application.CanStreamedLevelBeLoaded(winSceneName))
        {
            Application.LoadLevel(winSceneName);
        }
    }
    private void LoadLost()
    {
        if (Application.CanStreamedLevelBeLoaded(lostSceneName)) 
        {
            Application.LoadLevel(lostSceneName);
        } 
    }
    void OnDisable() 
    {
        if (CurrentLevelManager.InstanceInScene)
        {
            CurrentLevelManager.InstanceInScene.onLevelWin -= YouWIN;
            CurrentLevelManager.InstanceInScene.onLevelLost -= YouLOST;
        }
    }
}
