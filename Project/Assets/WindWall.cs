﻿using UnityEngine;
using System.Collections;

public class WindWall : MonoBehaviour {

	public Vector3 directionToPush;

	public float power;
	public float dividerVelocity;
	private int count=0;
	private Rigidbody2D death;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(count>0){
			death.AddForce(power*Time.deltaTime*directionToPush);
		}
	}

	void OnTriggerEnter2D(Collider2D mort){
		if(mort.gameObject.name=="Elevator"){
			death=mort.GetComponent<Rigidbody2D>();
			count++;
			if(count==1){
				Transform child=transform.GetChild(0);
				child.gameObject.SetActive(true);
				child.LookAt(transform.position+directionToPush);
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D mort){
		if(mort.gameObject.name=="Elevator"){
			count--;
			if(count==0){
				Transform child=transform.GetChild(0);
				child.gameObject.SetActive(false);
				mort.gameObject.GetComponent<Rigidbody2D>().velocity=dividerVelocity * mort.gameObject.GetComponent<Rigidbody2D>().velocity;
			}
		}
	}

}
