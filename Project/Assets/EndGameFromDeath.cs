﻿using UnityEngine;
using System.Collections;

public class EndGameFromDeath : MonoBehaviour {


	private float timer;
	// Use this for initialization
	void Start () {
		timer=Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad-timer>3){
			CurrentLevelManager.NotifyLevelLost();
		}
	}
}
