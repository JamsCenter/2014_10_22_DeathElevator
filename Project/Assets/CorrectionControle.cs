﻿using UnityEngine;
using System.Collections;

public class CorrectionControle : MonoBehaviour {

	public float correct;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float angle=transform.rotation.eulerAngles.z;
		Debug.Log("angle="+angle);
		if(angle>180)
			GetComponent<Rigidbody2D>().AddTorque(correct*Mathf.Atan(Time.deltaTime*(360-angle)/360));
		else
			GetComponent<Rigidbody2D>().AddTorque(-1*correct*Mathf.Atan(Time.deltaTime*angle/360));
		
	}

}
