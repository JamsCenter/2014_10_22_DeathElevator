﻿using UnityEngine;
using System.Collections;

public class FixeCameraZoneFocus : CameraFocusElement
{


    private bool _isActive;

    public bool IsFocusActivce
    {
        get { return _isActive; }
        set { _isActive = value;
        if (value) CameraFocus.AddFocus(this);
        else CameraFocus.RemoveFocus(this);
        }
    }

    public float priotityFocus = 10f;
    public float size = 10f;

    public Transform defaultPosition;
    public Vector3 lastPositionValide;
    private Transform playerInZone;
    public bool activeOnLoad;
    public void Start()
    {
        if (defaultPosition == null) defaultPosition = this.transform;
        lastPositionValide = defaultPosition.position;
        if (activeOnLoad) IsFocusActivce = true;
    }

    public void Update() {
        if (!playerInZone) return;
        else lastPositionValide = playerInZone.position;
    }


    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        playerInZone = col.transform;
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        playerInZone = null;
    }


    public override Vector3 GetCameraPosition()
    {
        return lastPositionValide;
    }

    public override float GetCameraSize()
    {
        return size;
    }

    public override float GetPriority()
    {
        return priotityFocus;
    }
}
