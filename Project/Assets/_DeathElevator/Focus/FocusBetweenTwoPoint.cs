﻿using UnityEngine;
using System.Collections;

public class FocusBetweenTwoPoint : CameraFocusElement {


    public float priotityFocus = 10f;

    public float distanceMax = 20f;
    public float sizeMax = 20f;

    public float distanceMin = 1f;
    public float sizeMin = 10f;

    public Transform player;
    public float pourcent = 0.6f;
    public Vector3 adjustement;
    public void Start()
    {
        if (!player)
        {
            GameObject gamo = GameObject.FindWithTag("Player");
            if (gamo != null) player = gamo.transform;
        }
        CameraFocus.AddFocus(this);
    }
    public void OnDestroy()
    {
        CameraFocus.RemoveFocus(this);
    }

    


    public override Vector3 GetCameraPosition()
    {
        if (player == null)
            return transform.position + adjustement;
        else
            return transform.position + adjustement + (player.position - transform.position) * pourcent;
    }

    public override float GetCameraSize()
    {
        float d = Vector3.Distance(player.position, transform.position);

        d = d - distanceMin;
        float pourcent = (d - distanceMin) / (distanceMax - distanceMin);

        return sizeMin + (sizeMax - sizeMin) * pourcent;
    }

    public override float GetPriority()
    {
        return priotityFocus;
    }
}
