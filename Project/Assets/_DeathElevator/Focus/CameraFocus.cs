﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
public class CameraFocus : MonoBehaviour {


    private static CameraFocus _instance;

    public static CameraFocus Instance
    {
        get { return _instance; }
        set { 
            if(_instance==null) _instance = value; }
    }



    public List<CameraFocusElement> focus = new List<CameraFocusElement>();
    public CameraFocusElement currentFocus;

    public delegate void AddedFocus(CameraFocus focus, CameraFocusElement newFocus);
    public static AddedFocus OnAddedFocus;
    public delegate void SwitchedFocus(CameraFocus focus, CameraFocusElement newFocus);
    public static SwitchedFocus OnSwitchedFocus;
    public delegate void DeletedFocus(CameraFocus focus, CameraFocusElement newFocus);
    public static DeletedFocus OnDeletedFocus;

    void Awake() 
    
    {
        Instance = this;
    }

    public bool Add(CameraFocusElement focusElement)
    {
        if (focusElement == null|| focus==null) return false;
        if (focus.Contains(focusElement)) return false;
        focus.Add(focusElement);

        
        CameraFocusElement next = GetFocusWithHigherPriority();
        if(OnAddedFocus!=null)
        OnAddedFocus(this,next);
        if(currentFocus!=next)
            if (OnSwitchedFocus != null)
                OnSwitchedFocus(this, next);

        currentFocus=next;

        return true;
    }

   
    public bool Remove(CameraFocusElement focusElement)
    {
        if (focusElement == null|| focus==null) return false;
        if (!focus.Contains(focusElement)) return false;
        focus.Remove(focusElement);
         CameraFocusElement next = GetFocusWithHigherPriority();

         if (currentFocus != next)
             if (OnSwitchedFocus != null)
                 OnSwitchedFocus(this, next);
         if (OnDeletedFocus != null)
             OnDeletedFocus(this, next);

        currentFocus=next;

        return true;
    }
    private CameraFocusElement GetFocusWithHigherPriority()
    {
        var result = from f in focus orderby f.GetPriority() descending select f;
        if (result == null && result.Count<CameraFocusElement>() <= 0) return null;
        return result.First<CameraFocusElement>();
    }


    public static bool AddFocus(CameraFocusElement playerCameraFocus)
    {
        if (Instance == null) return false;
       return Instance.Add(playerCameraFocus);
    }
    public static bool RemoveFocus(CameraFocusElement playerCameraFocus)
    {
        if (Instance == null) return false;
        return Instance.Remove(playerCameraFocus);
    }
}

public interface ICameraFocusElement
{
    Vector3 GetCameraPosition();
    float GetCameraSize();
    float GetPriority();

}
public abstract class CameraFocusElement : MonoBehaviour, ICameraFocusElement
{
    public abstract Vector3 GetCameraPosition();
    public abstract float GetCameraSize();
    public abstract float GetPriority();
}

