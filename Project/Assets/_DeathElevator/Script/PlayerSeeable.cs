﻿using UnityEngine;
using System.Collections;

public class PlayerSeeable : MonoBehaviour
{
    public Camera camerari;
    public Transform target;
    public float timeStayOut;
    public float maxTimeOut=4;
    public bool maxTimeNotify;

    public delegate void OnIsOutOfView(PlayerSeeable origine, Vector3 viewPointPort);
    public delegate void OnIsOutOfViewSinceTooLong(PlayerSeeable origine, Vector3 viewPointPort);
    public delegate void OnRegainView(PlayerSeeable origine);
    
    public OnIsOutOfView onIsOutOfView;
    public OnIsOutOfViewSinceTooLong onIsOutOfViewSinceTooLong;
    public OnRegainView onRegainView;

    public void Awake() {
        if (camerari == null) camerari = Camera.main;
    }
    void Update()
    {
        Vector3 viewPos = camerari.WorldToViewportPoint(target.position);
        if (viewPos.x < 0 || viewPos.x > 1 || viewPos.y < 0 || viewPos.y > 1)
        {
            if (timeStayOut == 0) {
                if (onIsOutOfView != null)
                    onIsOutOfView(this, viewPos);

             //start to be out
            }
            timeStayOut += Time.deltaTime;

            if (!maxTimeNotify && timeStayOut > maxTimeOut  ) {
                maxTimeNotify = true;
                print("Out for too long");
                if (onIsOutOfViewSinceTooLong != null)
                    onIsOutOfViewSinceTooLong(this, viewPos);
            }
        }
        else
        {
            if (timeStayOut > 0)
            {
                maxTimeNotify = false;
                if (onRegainView != null)
                    onRegainView(this);
            }
            timeStayOut = 0;
        }
    }
}