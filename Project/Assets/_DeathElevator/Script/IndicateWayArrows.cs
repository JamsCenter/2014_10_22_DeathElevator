﻿using UnityEngine;
using System.Collections;

public class IndicateWayArrows : MonoBehaviour {

    private static IndicateWayArrows InstanceInScene;

    public enum Direction { Left, Right, Top, Bot}
    public Transform arrowLeft;
    public Transform arrowRight;
    public Transform arrowTop;
    public Transform arrowBot;

    void Awake () {
        InstanceInScene = this;
        ShowOffAll();
	}

    public static void ShowArrow(Direction direction, bool withDeactiveAll=true) 
    {
        IndicateWayArrows indic = InstanceInScene;
        if (!indic) return ;
        if(withDeactiveAll)
        indic.ShowOffAll();
        switch (direction)
        {
            case Direction.Left: indic.arrowLeft.gameObject.SetActive(true); break;
            case Direction.Right: indic.arrowRight.gameObject.SetActive(true); break;
            case Direction.Top: indic.arrowTop.gameObject.SetActive(true); break;
            case Direction.Bot: indic.arrowBot.gameObject.SetActive(true); break;
        }
    }

    public static void StopDisplay()
    {
        IndicateWayArrows indic = InstanceInScene;
        if (!indic) return;
        indic.ShowOffAll();
    
    }
    public void ShowOffAll()
    {
        if (arrowLeft) arrowLeft.gameObject.SetActive(false);
        if (arrowRight) arrowRight.gameObject.SetActive(false);
        if (arrowTop) arrowTop.gameObject.SetActive(false);
        if (arrowBot) arrowBot.gameObject.SetActive(false);
    
    }

    public  static void AddArrow(Direction direction)
    {
        ShowArrow(direction, false);
    }
}
