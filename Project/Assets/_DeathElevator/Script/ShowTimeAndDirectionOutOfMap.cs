﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowTimeAndDirectionOutOfMap : MonoBehaviour {


    public PlayerSeeable seeable;

    public void OnEnable()
    {
        seeable.onIsOutOfView += ShowOutOfViewInfo;
        seeable.onRegainView += ShowOff;
    }
    public void OnDisable()
    {

        seeable.onIsOutOfView -= ShowOutOfViewInfo;
        seeable.onRegainView -= ShowOff;
    }

    private void ShowOff(PlayerSeeable origine)
    {
        IndicateWayArrows.StopDisplay();
    }
    private void ShowOutOfViewInfo(PlayerSeeable origine, Vector3 viewPointPort)
    {

        if (viewPointPort.x > 1) IndicateWayArrows.AddArrow(IndicateWayArrows.Direction.Right);
        if (viewPointPort.x < 0) IndicateWayArrows.AddArrow(IndicateWayArrows.Direction.Left);
        if (viewPointPort.y > 1) IndicateWayArrows.AddArrow(IndicateWayArrows.Direction.Top);
        if (viewPointPort.y < 0) IndicateWayArrows.AddArrow(IndicateWayArrows.Direction.Bot);

    }
    
	void Update () {
        
	
	}
}
