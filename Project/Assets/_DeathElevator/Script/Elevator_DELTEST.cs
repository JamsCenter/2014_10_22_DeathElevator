﻿using UnityEngine;
using System.Collections;

public class Elevator_DELTEST : MonoBehaviour {

    public float power = 10f;
	
	void Update () {

        if (Input.GetKey(KeyCode.UpArrow)) 
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up*power*Time.deltaTime);
        
        }
	}
}
