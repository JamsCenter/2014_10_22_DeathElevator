﻿using UnityEngine;
using System.Collections;

public class MeetPoint : MonoBehaviour {

    private bool _active;

    public bool Activate
    {
        get { return _active; }
        set {
            if (_active == value) return;
            _active = value;
            NotifyActiveChange(_active);
        }
    }

    private void NotifyActiveChange(bool onOff)
    {
        if (onMeetActivated != null)
            onMeetActivated(this, onOff);
    }

    public float range =10f;
    public Transform where;
    public bool debug=true;


    public void Awake() {
        if (where == null) where = this.transform;
        if (debug)
            onMeetActivated += DrawDebugZone;
    }

   

    public delegate void OnMeetActivated(MeetPoint meetingInfo, bool onOff);
    public static OnMeetActivated onMeetActivated;

    private static float drawLineTime = 3f;
    private static Color drawColorOn = Color.green;
    private static Color drawColorOff = Color.red;
    private static void DrawDebugZone(MeetPoint meetingInfo, bool onOff)
    {
        float range = meetingInfo.range;
        Vector3 left, right, top, bot;
        left = right = top = bot = meetingInfo.where.position;
        left.x -= range;
        right.x += range;
        top.x += range;
        bot.x -= range;
        Debug.DrawLine(left, right, onOff ? drawColorOn : drawColorOff, drawLineTime);
        Debug.DrawLine(bot, top, onOff ? drawColorOn : drawColorOff, drawLineTime);
    }
   
	
}
