﻿using UnityEngine;
using System.Collections;

public class SetWhereToGoWhenMeetingActiated : MonoBehaviour {

    public MoveAtTransform moveScriptManager;

    void OnEnable() 
    {
        MeetPoint.onMeetActivated += SetWhereToMove;
    } 

	void OnDisable()
    {
        MeetPoint.onMeetActivated -= SetWhereToMove;
    }

	private void SetWhereToMove(MeetPoint meetingInfo, bool onOff)
	{
	    if(moveScriptManager==null)
			return;
	    
		if (onOff) 
		{
	        float distance = Vector3.Distance(meetingInfo.where.position, moveScriptManager.transform.position);
	        if(distance<meetingInfo.range)
	        moveScriptManager.goAt = meetingInfo.where;
	        
	    }
	    
	}
   

}
