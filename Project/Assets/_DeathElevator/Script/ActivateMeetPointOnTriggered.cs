﻿using UnityEngine;
using System.Collections;

public class ActivateMeetPointOnTriggered : MonoBehaviour {

    public MeetPoint meetPoint;


    public void OnTriggerEnter2D(Collider2D col)
    {
        ActivateMeetIfElevator(col, true);

    }
    public void OnTriggerExit2D(Collider2D col)
    {
        ActivateMeetIfElevator(col, false);
    }

    private void ActivateMeetIfElevator(Collider2D col, bool onOff)
    {
        if (meetPoint == null) return;
        if (!col.gameObject.name.Contains("Elevator")) return;
        Debug.Log(col.gameObject.name);
        meetPoint.Activate = onOff;
    }

}
