﻿using UnityEngine;
using System.Collections;

public class StayOnPlatForm : MonoBehaviour {

	public MoveAtTransform movescript;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D platform){
		if(platform.gameObject.name=="Elevator"){
			movescript.goAt=platform.gameObject.transform;
			movescript.distanceToBeThere=0.0f;
		}
	}
}
