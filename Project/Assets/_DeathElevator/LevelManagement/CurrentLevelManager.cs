﻿using UnityEngine;
using System.Collections;

public class CurrentLevelManager : MonoBehaviour {

    private static CurrentLevelManager _instanceInScene;

     static CurrentLevelManager ()
    {
        
    }

     public static CurrentLevelManager InstanceInScene
     {
         get
         {
             if (_instanceInScene == null)
             {
                 _instanceInScene = GameObject.FindObjectOfType<CurrentLevelManager>() as CurrentLevelManager;
             }
             return _instanceInScene;
         }
         set
         {
             _instanceInScene = value;
         }
     }


    public delegate void OnLevelTimeout();
    public delegate void OnLevelLostDetected(float timeLeft);
    public delegate void OnLevelWinDetected(float timeLeft);
    public delegate void OnLevelEnd(float timeLeft);

    public  OnLevelTimeout    onTimeout;
    public  OnLevelLostDetected onLevelLost;
    public  OnLevelWinDetected onLevelWin;
    public  OnLevelEnd onLevelEnd;

    private bool hasBeenInit;
    public  bool hasWin;
    public  bool hasLost;

    public bool debug;


    public void Awake()
    {
        //Debug.Log("Refresh");
        Init();
    }

    

    private  void Init()
    {
        if (hasBeenInit) return;
        hasBeenInit = true;
        //Debug.Log("Hi init",this.gameObject);
        //onLevelLost = null;
        //onLevelWin = null;
        hasWin=false;
        hasLost = false;
        InstanceInScene = this;

    }

    public static void NotifyLevelTimeout() {

        if (InstanceInScene.onTimeout != null)
            InstanceInScene.onTimeout();
        NotifyLevelLost();

    }
    public static void NotifyLevelLost()
    {
        if (InstanceInScene == null)
        {
            Debug.Log("Not level manager in scene");
            return;
        }
        if (LevelEnded()) return;

      //  Debug.Log("Noti. lost");
        if (InstanceInScene.onLevelLost != null )
            InstanceInScene.onLevelLost(LevelTimer.InstanceInScene?LevelTimer.InstanceInScene.GetTimeLeft():0);

        if (InstanceInScene.onLevelEnd != null )
            InstanceInScene.onLevelEnd(LevelTimer.InstanceInScene ? LevelTimer.InstanceInScene.GetTimeLeft() : 0);
        InstanceInScene.hasLost = true;
    }

    private static bool LevelEnded()
    {
        return InstanceInScene.hasLost || InstanceInScene.hasWin;
    }
    public static void NotifyLevelWin()
    {
        if (InstanceInScene == null)
        {
            Debug.Log("Not level manager in scene");
            return;
        }

        if (LevelEnded()) return;
       // Debug.Log("Noti. Win" );
        if (InstanceInScene.onLevelWin != null)
            InstanceInScene.onLevelWin(LevelTimer.InstanceInScene ? LevelTimer.InstanceInScene.GetTimeLeft() : 0);

        if (InstanceInScene.onLevelEnd != null)
            InstanceInScene.onLevelEnd(LevelTimer.InstanceInScene ? LevelTimer.InstanceInScene.GetTimeLeft() : 0);
        InstanceInScene.hasWin = true;
    }

    public void OnEnable()
    {

        if (debug) { 
            onLevelWin += DisplayWinDebug;
            onLevelLost += DisplayLostDebug;
            onTimeout += DisplayTimeoutDebug;
        }

    }
    public void OnDisable()
    {
        if (debug)
        {

            onLevelWin -= DisplayWinDebug;
            onLevelLost -= DisplayLostDebug;
            onTimeout -= DisplayTimeoutDebug;

        }
        Destroy(this.gameObject);

    }

    private void DisplayTimeoutDebug()
    {
        Debug.Log("Time out !  " );
    }

    private void DisplayLostDebug(float timeLeft)
    {
        Debug.Log("You lose !  " + timeLeft);
    }

    private void DisplayWinDebug(float timeLeft)
    {
        Debug.Log("You win !  " + timeLeft);
    }

 
}
