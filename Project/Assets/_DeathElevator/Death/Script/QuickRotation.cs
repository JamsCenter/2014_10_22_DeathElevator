﻿using UnityEngine;
using System.Collections;

public class QuickRotation : MonoBehaviour {


    public float rotationPerSecond = 30f;
	public float multiplicateur =1f;

	void Update () {

		transform.Rotate(Vector3.forward * (multiplicateur * rotationPerSecond * Time.deltaTime));
	
	}
}
