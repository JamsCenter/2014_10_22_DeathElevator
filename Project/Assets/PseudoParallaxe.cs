﻿using UnityEngine;
using System.Collections;

public class PseudoParallaxe : MonoBehaviour {

    public float maxHorizontalMove = 3f;
    public float horizontalMoveOfByUnity = 0.3f;
    public float maxVerticalMove = 1f;
    public float verticalMoveOfByUnity = 0.1f;

    public Transform target;
    public Vector3 targetStartPoint;
    public Vector3 thisStartPoint;

	void Start () {
        if (target == null) { Destroy(this); return; }
        targetStartPoint = target.position;
        thisStartPoint = transform.position;
	    
	}
	
	void Update () {

        Vector2 directionNN = target.position-targetStartPoint;
        Vector3 pos = thisStartPoint;
        pos.x += Mathf.Clamp(directionNN.x * horizontalMoveOfByUnity,-maxHorizontalMove,maxHorizontalMove);
        pos.y += Mathf.Clamp(directionNN.y * verticalMoveOfByUnity, -maxHorizontalMove, maxHorizontalMove);

        transform.position = pos;
	}
}
