﻿using UnityEngine;
using System.Collections;

public class S_Menu : MonoBehaviour {

	public Texture2D playtex;
	public Texture2D credtex;
	public Texture2D exittex;
	public Texture2D highlighttex;
	
	private Rect playrect=new Rect(0.362F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
	private Rect credrect=new Rect(0.571F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
	private Rect exitrect=new Rect(0.777F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);

	bool init=false;

	private int highlight=0;

	private float lastchange=0f;

	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Joystick1Button0)){
			switch(highlight){
			case 0:
				Application.LoadLevel("Level_1");
				break;
			case 1:
				Application.LoadLevel("Credit");
				break;
			case 2:
				Application.Quit();
				break;
			}
		}

		if(Time.timeSinceLevelLoad - lastchange > 0.25f){
			if(Input.GetAxis("JLH")+Input.GetAxis("JRH")>0.95){
				highlight=(highlight+1)%3;
				lastchange=Time.timeSinceLevelLoad;
			}else if(Input.GetAxis("JLH")+Input.GetAxis("JRH")<-0.95){
				highlight=(highlight+3-1)%3;
				lastchange=Time.timeSinceLevelLoad;
			}
		}	
	}

	void OnGUI(){
		playrect=new Rect(0.362F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
		credrect=new Rect(0.571F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
		exitrect=new Rect(0.777F*Screen.width,0.636F*Screen.height,0.157F*Screen.width,0.271F*Screen.height);
		if(!init){
			init=true;
			GUI.skin.button=GUIStyle.none;
		}
		if(GUI.Button(playrect,playtex)){
			Application.LoadLevel("Level_1");
		}
		if(GUI.Button(credrect,credtex)){
			Application.LoadLevel("Credit");
		}
		if(GUI.Button(exitrect,exittex)){
			Application.Quit();
		}

		switch(highlight){
		case 0:
			GUI.Button(playrect,highlighttex);
			break;
		case 1:
			GUI.Button(credrect,highlighttex);
			break;
		case 2:
			GUI.Button(exitrect,highlighttex);
			break;
		}

	}
}
