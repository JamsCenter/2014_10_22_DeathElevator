﻿using UnityEngine;
using System.Collections;

public class roukill : MonoBehaviour {


	public GameObject father;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position=father.transform.position;
		transform.rotation=father.transform.rotation;
	}

	void OnTriggerEnter2D(Collider2D lemming){
		if(lemming.gameObject.name=="Soul"){
			lemming.gameObject.GetComponent<KillableSoul>().kill();
		}
	}
}
